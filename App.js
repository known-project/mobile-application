/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Modal, Button } from "react-native";
import { connect } from "react-redux";

import {
  addTimeSpan,
  selectTimeSpan,
  deselectTimeSpan,
  deleteTimeSpan
} from "./src/store/actions/timeTable";

import SpanTable from "./src/components/SpanTable/SpanTable";

class App extends Component {
// class App extends Component {
  render() {
    return (
      <View style={styles.wrapAll}>
        <SpanTable />
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.props.selectedSpan !== null}
          onRequestClose={this.props.onDeselectTimeSpan}
        >
          <View>
            <Text>Some Text</Text>
            <Button title={"Close"} onPress={this.props.onDeselectTimeSpan} />
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapAll: {
      height: "100%"
  }
});

const mapStateToProps = state => {
  return {
      timeSpans: state.timeTable.timeSpans,
      selectedSpan: state.timeTable.selectedSpan
  };
};

const mapDispatchToProps = dispatch => {
  return {
      onAddTimeSpan: spanTitle => dispatch(addTimeSpan(spanTitle)),
      onSelectTimeSpan: spanKey => dispatch(selectTimeSpan(spanKey)),
      onDeselectTimeSpan: () => dispatch(deselectTimeSpan()),
      onDeleteTimeSpan: spanKey => dispatch(deleteTimeSpan(spanKey))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
