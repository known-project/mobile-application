import {
  ADD_SPAN,
  SELECT_SPAN,
  DESELECT_SPAN,
  DELETE_SPAN
} from "./actionTypes";

export const addTimeSpan = spanTitle => {
  return {
    type: ADD_SPAN,
    spanTitle: spanTitle
  };
};

export const selectTimeSpan = key => {
  return {
    type: SELECT_SPAN,
    spanKey: key
  };
};

export const deselectTimeSpan = () => {
  return {
    type: DESELECT_SPAN
  };
};

export const deleteTimeSpan = key => {
  return {
    type: DELETE_SPAN,
    spanKey: key
  };
};
