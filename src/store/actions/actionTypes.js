// adding span
export const ADD_SPAN = "ADD_SPAN";

// selecting span
export const SELECT_SPAN = "SELECT_SPAN";

// deselect span
export const DESELECT_SPAN = "DESELECT_SPAN";

// deleting span
export const DELETE_SPAN = "DELETE_SPAN";

