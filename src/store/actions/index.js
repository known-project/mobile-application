export {
  addTimeSpan,
  selectTimeSpan,
  deselectTimeSpan,
  deleteTimeSpan
} from "./timeTable";
