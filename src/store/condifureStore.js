import { createStore, combineReducers } from "redux";

import timeTableReducer from "./reducers/timeTable";

const rootReducer = combineReducers({
  timeTable: timeTableReducer
});

const configureStore = () => {
  return createStore(rootReducer);
};

export default configureStore;

