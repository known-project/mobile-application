import {
    ADD_SPAN,
    SELECT_SPAN,
    DESELECT_SPAN,
    DELETE_SPAN
} from "../actions/actionTypes";

const reducer = (state = null, action) => {
    if (state === null) {
        state = {
            timeSpans: [{
                key: 12,
                spanTitle: 'title',
                start: new Date(0, 0, 0, 0, 15, 10),
                end: new Date(0, 0, 0, 1, 30, 12),
                bgColor: "#79ee7e",
                fgColor: "#ee496b"
            }],
            selectedSpan: null
        };
    }

    switch (action.type) {
        case ADD_SPAN:
            return {
                ...state,
                timeSpans: state.timeSpans.concat({
                    key: Math.random(),
                    spanTitle: action.spanTitle
                })
            };
        case SELECT_SPAN:
            return {
                ...state,
                selectedSpan: state.timeSpans.find(item => item.key === action.spanKey)
            };
        case DESELECT_SPAN:
            return {
                ...state,
                selectedSpan: null
            };
        case DELETE_SPAN:
            return {
                ...state,
                timeSpans: state.timeSpans.filter(item => item.key !== action.spanKey),
                selectedSpan: null
            };
        default:
            return state;
    }
};

export default reducer;
