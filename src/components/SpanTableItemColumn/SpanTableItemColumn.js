import React from "react";
import {View, StyleSheet} from "react-native";

import SpanTableItem from "./SpanTableItem/SpanTableItem";
import SpanTableItemSpacer from "./SpanTableItemSpacer/SpanTableItemSpacer";
import {addTimeSpan, deleteTimeSpan, deselectTimeSpan, selectTimeSpan} from "../../store/actions";
import {connect} from "react-redux";

class SpanTableItemColumn extends React.Component {
    styles = StyleSheet.create({
        perDayColumn: {
            width: 160,
            borderLeftWidth: 1,
            borderColor: "gray"
        }
    });

    render() {
        return (
            <View style={[this.styles.perDayColumn]}>
                <SpanTableItemSpacer heightPerHour={this.props.heightPerHour} minutes={100}/>
                <SpanTableItem heightPerHour={this.props.heightPerHour}
                               minutes={100}
                               span={this.props.timeSpans[0]}
                               spanName="2 some other text"/>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        timeSpans: state.timeTable.timeSpans,
        selectedSpan: state.timeTable.selectedSpan
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddTimeSpan: spanTitle => dispatch(addTimeSpan(spanTitle)),
        onSelectTimeSpan: spanKey => dispatch(selectTimeSpan(spanKey)),
        onDeselectTimeSpan: () => dispatch(deselectTimeSpan()),
        onDeleteTimeSpan: spanKey => dispatch(deleteTimeSpan(spanKey))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SpanTableItemColumn);
