import React from "react";
import {View, StyleSheet} from "react-native";

export default class SpanTableItemSpacer extends React.Component {
    styles = (totalHeight) => StyleSheet.create({
        itemSpacer: {
            backgroundColor: 'transparent',
            height: totalHeight
        }
    });

    render() {
        const heightTotal = this.props.heightPerHour * this.props.minutes / 60;
        return (
            <View style={this.styles(heightTotal).itemSpacer}>
            </View>
        );
    }
}
