import React from "react";
import {View, Text, StyleSheet, TouchableHighlight} from "react-native";
import {addTimeSpan, deleteTimeSpan, deselectTimeSpan, selectTimeSpan} from "../../../store/actions";
import {connect} from "react-redux";


class SpanTableItem extends React.Component {
    styles = StyleSheet.create({
        item: {
            padding: 10,
            flexDirection: "column",
            justifyContent: "flex-start",
        },
        itemTextTitle: {
            fontWeight: "bold",
            fontSize: 32
        }
    });

    render() {
        const currentSpan = this.props.span;
        let totalHours = currentSpan.end.getHours() - currentSpan.start.getHours();
        totalHours += (currentSpan.end.getMinutes() - currentSpan.start.getMinutes()) / 60;
        const heightTotal = this.props.heightPerHour * totalHours;

        const itemStyle = StyleSheet.create({
            item: {
                backgroundColor: currentSpan.bgColor,
                height: heightTotal
            },
            itemText: {
                color: currentSpan.fgColor
            }
        });

        return (
            <TouchableHighlight onPress={() => this.props.onSelectTimeSpan(12)}>
                <View style={[this.styles.item, itemStyle.item]}>
                    <Text style={[this.styles.itemTextTitle, itemStyle.itemText]}>{currentSpan.spanTitle}</Text>
                    <Text>{this.props.spanName}</Text>
                    <Text>{this.props.spanName}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}

const mapStateToProps = state => {
    return {
        timeSpans: state.timeTable.timeSpans,
        selectedSpan: state.timeTable.selectedSpan
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddTimeSpan: spanTitle => dispatch(addTimeSpan(spanTitle)),
        onSelectTimeSpan: spanKey => dispatch(selectTimeSpan(spanKey)),
        onDeselectTimeSpan: () => dispatch(deselectTimeSpan()),
        onDeleteTimeSpan: spanKey => dispatch(deleteTimeSpan(spanKey))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SpanTableItem);