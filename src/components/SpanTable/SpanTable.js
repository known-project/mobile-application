import React from "react";
import {ScrollView, View, Text, StyleSheet} from "react-native";

import SpanTableItemColumn from "../SpanTableItemColumn/SpanTableItemColumn";
import SpanTableTimeColumn from "../SpanTableTimeColumn/SpanTableTimeColumn";

export default class SpanTable extends React.Component {
    styles = StyleSheet.create({
        itemBoxScrollDayLong: {
            flex: 1,
            flexDirection: "column"
        },
        horizontalScroll: {
            flex: 1
        },
        dayLableHeight: {
            height: 60
        },
        dayLabel: {
            borderColor: "black",
            borderWidth: 1
        },
        mainLabel: {
            borderColor: "black",
            borderWidth: 1
        },
        timeLineColumn: {
            width: 100
        },
        perDayColumn: {
            width: 160
        }
    });

    heightPerHour = 100;

    render() {
        return (
            <ScrollView style={this.styles.itemBoxScrollDayLong}>
                <View style={{flex: 1, flexDirection: "row"}}>
                    {/* view of the left-most fixed pane */}
                    <SpanTableTimeColumn heightPerHour={this.heightPerHour}/>

                    {/* view of the h scolling pane <<<Day Wise>>> */}
                    <ScrollView horizontal={true} style={this.styles.horizontalScroll}>
                        <View style={{flexDirection: "column"}}>
                            <View style={{flexDirection: "row", height: 60}}>
                                <View
                                    style={[
                                        this.styles.dayLabel,
                                        this.styles.perDayColumn,
                                        this.styles.dayLableHeight
                                    ]}
                                >
                                    <Text>D</Text>
                                </View>
                                <View
                                    style={[
                                        this.styles.dayLabel,
                                        this.styles.perDayColumn,
                                        this.styles.dayLableHeight
                                    ]}
                                >
                                    <Text>D</Text>
                                </View>
                                <View
                                    style={[
                                        this.styles.dayLabel,
                                        this.styles.perDayColumn,
                                        this.styles.dayLableHeight
                                    ]}
                                >
                                    <Text>D</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: "row"}}>
                                <SpanTableItemColumn heightPerHour={this.heightPerHour}/>
                                <SpanTableItemColumn heightPerHour={this.heightPerHour}/>
                                <SpanTableItemColumn heightPerHour={this.heightPerHour}/>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}
