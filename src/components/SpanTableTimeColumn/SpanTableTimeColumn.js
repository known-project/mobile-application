import React from "react";
import {View, Text, StyleSheet} from "react-native";

import SpanTableTimeItem from "./SpanTableTimeItem/SpanTableTimeItem";

export default class SpanTableTimeColumn extends React.Component {
    styles = StyleSheet.create({
        itemBoxScrollDayLong: {
            flex: 1,
            flexDirection: "column"
        },
        dayLabelHeight: {
            height: 60
        },
        mainLabel: {
            borderColor: "black",
            borderWidth: 0
        },
        timeLineColumn: {
            width: 50,
            borderRightWidth: 2,
            borderColor: "black"
        }
    });

    render() {
        const height = this.props.heightPerHour;
        const hours = [...Array(23).keys()];
        const timeItems = hours.map((hour) => {
            const spanTitle = `${(hour + 1).toString()}:00`;
            return <SpanTableTimeItem key={hour} height={height} spanName={spanTitle}/>
        });

        return (
            <View style={[this.styles.timeLineColumn]}>
                <View
                    style={[
                        this.styles.mainLabel,
                        // this.styles.timeLineColumn,
                        this.styles.dayLabelHeight
                    ]}
                >
                    <Text>D</Text>
                </View>

                <SpanTableTimeItem height={height} firstItem />
                {timeItems}
                <SpanTableTimeItem height={height} firstItem />

            </View>
        );
    }
}