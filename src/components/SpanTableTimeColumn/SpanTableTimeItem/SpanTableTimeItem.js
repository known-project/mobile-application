import React from "react";
import {View, Text, StyleSheet} from "react-native";


export default class SpanTableTimeItem extends React.Component {
    styles = StyleSheet.create({
        itemWrapper: {
            borderColor: "blue",
            borderWidth: 0,
            flexDirection: "column",
            justifyContent: "space-around",
            backgroundColor: "gray",
        },
        textFormat: {
            fontSize: 18,
            fontWeight: "normal",
            textAlign: "right"
        }
    });

    render() {
        let height = 120;
        if (this.props.hasOwnProperty("height"))
            height = this.props.height;
        if (this.props.hasOwnProperty('firstItem'))
            height /= 2;

        const heightStyle = StyleSheet.create({
            height: height
        });
        if (this.props.hasOwnProperty('firstItem')) {
            return <View style={[this.styles.itemWrapper, heightStyle]}/>;
        }

        return (
            <View style={[this.styles.itemWrapper, heightStyle]}>
                <Text style={[this.styles.textFormat]}>{this.props.spanName}</Text>
            </View>
        );
    }

}